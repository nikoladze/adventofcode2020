#include <stdio.h>
#include <stdlib.h>

#define COMB_BUFSIZE 16
#define NUMBERS_BUFSIZE 4096
#define YEAR 2020
#define INPUT_FILENAME "input"

int find_combinations(int combinations_buffer[COMB_BUFSIZE], size_t n_comb,
                      int numbers[NUMBERS_BUFSIZE], size_t n_numbers,
                      int n,
                      int year) {
  if ((n >= n_comb) || (n > n_comb)) {
    return -1;
  }
  int sum, i, j;
  for (i = 0; i < n_numbers; ++i) {
    combinations_buffer[n] = numbers[i];
    if (n == (n_comb - 1)) {
      sum = 0;
      for (j = 0; j < n_comb; ++j) {
        sum += combinations_buffer[j];
      }
      if (sum == year) {
        return 0;
      }
    } else {
      if (find_combinations(combinations_buffer, n_comb,
                            numbers, n_numbers,
                            n + 1,
                            year) == 0) {
        return 0;
      }
    }
  }
  return 1;
}

int main(int argc, char *argv[]) {
  /* parse arguments */
  if (argc != 2) {
    printf("Need to provide how many numbers to combine\n");
    return 1;
  }
  int n = atoi(argv[1]);
  if (n >= COMB_BUFSIZE) {
    printf("Maximum value allowed: %d\n", COMB_BUFSIZE);
    return 1;
  }

  int combinations_buffer[COMB_BUFSIZE];
  int numbers[NUMBERS_BUFSIZE];

  /* read numbers from file into numbers array */
  FILE *fp = fopen(INPUT_FILENAME, "r");
  if (!fp) {
    printf("Can't open file with filename \"%s\"\n", INPUT_FILENAME);
    return 1;
  }
  int n_numbers = 0;
  while (fscanf(fp, "%d\n", &numbers[n_numbers]) != -1) {
    if (n_numbers >= NUMBERS_BUFSIZE) {
      printf("File too large\n");
      return -1;
    }
    n_numbers++;
  }

  /* find combination */
  int res = find_combinations(combinations_buffer, n, numbers, n_numbers, 0, YEAR);
  if (res == 0) {
    unsigned long long prod = 1;
    printf("Numbers: ");
    for (int i = 0; i < n; ++i) {
      prod *= (unsigned long long)combinations_buffer[i];
      printf("%d ", combinations_buffer[i]);
    }
    printf("\n");
    printf("Product: %llu\n", prod);
  } else if (res == -1) {
    printf("Inputs out of bounds\n");
  } else {
    printf("No combination found\n");
  }

  return 0;
}
