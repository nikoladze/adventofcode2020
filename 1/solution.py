#!/usr/bin/env python

import awkward1 as ak
import numpy as np

numbers = ak.Array(np.loadtxt("input", dtype=np.int64))
combinations = ak.combinations(numbers, 2, axis=0)
n1, n2 = ak.unzip(combinations)
print(f"Part1: {ak.prod(combinations[n1 + n2 == 2020]):.0f}")

combinations_3 = ak.combinations(numbers, 3, axis=0)
n1, n2, n3 = ak.unzip(combinations_3)
print(f"Part2: {ak.prod(combinations_3[n1 + n2 + n3 == 2020]):.0f}")
