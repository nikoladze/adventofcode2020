#!/usr/bin/env python

from operator import mul
from functools import reduce

numbers = []
with open("input") as f:
    for l in f:
        numbers.append(int(l.strip()))

def find_2combination_prod(numbers):
    for i in numbers:
        for j in numbers:
            if i == j:
                continue
            if i + j == 2020:
                return i * j

print(f"Part1: {find_2combination_prod(numbers)}")

def find_ncombination_prod(numbers, n):

    def get_combinations(combinations, n):
        for i in numbers:
            if i in combinations:
                continue
            new_combinations = combinations + [i]
            if n == 0:
                if sum(combinations) + i == 2020:
                    return combinations + [i]
            else:
                ret = get_combinations(new_combinations, n - 1)
                if ret is not None:
                    return ret

    return reduce(mul, get_combinations([], n - 1), 1)

print(f"Part2: {find_ncombination_prod(numbers, 3)}")

print(f"Combinations of 4: {find_ncombination_prod(numbers, 4)}")
