#!/usr/bin/env python

import numpy as np


def diffs_extended(adapters):
    adapters = np.array(adapters)
    return np.diff(np.concatenate([[0], np.sort(adapters), [adapters.max() + 3]]))


def solve_part1(adapters):
    adapters = np.array(adapters)
    diffs = diffs_extended(adapters)
    return (diffs == 1).sum() * (diffs == 3).sum()


adapters_example1 = [
    28, 33, 18, 42, 31, 14, 46, 20, 48, 47, 24, 23, 49, 45, 19, 38, 39, 11, 1,
    32, 25, 35, 8, 17, 7, 9, 4, 2, 34, 10, 3,
]


def test_part1():
    assert solve_part1(adapters_example1) == 22 * 10


combination_dict = {
    0: 1,
    1: 1,
    # 1 + 1, 2
    2: 2,
    # 1 + 1 + 1, 1 + 2, 2 + 1, 3
    3: 4,
    # 1 + 1 + 1 + 1, 2 + 1 + 1 (3 times), 3 + 1 (2 times), 2 + 2
    4: 7,
}


def solve_part2(adapters):
    diffs = diffs_extended(adapters)
    list_of_one_sequences = diffs.astype(np.int8).tobytes().decode().split("\x03")
    return np.prod([combination_dict[len(i)] for i in list_of_one_sequences])


def test_part2():
    assert solve_part2(adapters_example1) == 19208


if __name__ == "__main__":

    adapters = np.loadtxt("input")
    print(solve_part1(adapters))
    print(solve_part2(adapters))
