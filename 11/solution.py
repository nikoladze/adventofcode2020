#!/usr/bin/env python

import numpy as np
import pytest


class Ferry:
    def __init__(self, initial_seats, mode=1):
        self.seats = np.array(initial_seats)
        self.mode = mode
        if self.mode == 1:
            self.threshold = 4
        elif self.mode == 2:
            self.threshold = 5
        else:
            raise NotImplementedError(f"Mode {mode} not implemented")

    def n_occupied_around(self, i, j):
        if self.mode == 1:
            return self.n_occupied_adjacent(i, j)
        elif self.mode == 2:
            return self.n_occupied_in_sight(i, j)

    def n_occupied_in_sight(self, i, j):
        n = 0
        for k in range(-1, 2):
            for l in range(-1, 2):
                if k == l == 0:
                    continue
                pos = (i, j)
                while True:
                    pos = (pos[0] + k, pos[1] + l)
                    if not (
                        (0 <= pos[0] < self.seats.shape[0])
                        and (0 <= pos[1] < self.seats.shape[1])
                    ):
                        break
                    if self.seats[pos[0], pos[1]] == "L":
                        break
                    if self.seats[pos[0], pos[1]] == "#":
                        n += 1
                        break
        return n

    def n_occupied_adjacent(self, i, j):
        n = 0
        for k in range(i - 1, i + 2):
            for l in range(j - 1, j + 2):
                if (
                    (k < 0)
                    or (k >= self.seats.shape[0])
                    or (l < 0)
                    or (l >= self.seats.shape[1])
                    or (k == i and l == j)
                ):
                    continue
                if self.seats[k][l] == "#":
                    n += 1
        return n

    def step(self):
        updated_seats = np.array(self.seats)
        for i in range(self.seats.shape[0]):
            for j in range(self.seats.shape[1]):
                if self.seats[i][j] == ".":
                    continue
                if self.seats[i][j] == "L" and self.n_occupied_around(i, j) == 0:
                    updated_seats[i][j] = "#"
                    continue
                if (
                    self.seats[i][j] == "#"
                    and self.n_occupied_around(i, j) >= self.threshold
                ):
                    updated_seats[i][j] = "L"
                    continue
        state_changed = (self.seats != updated_seats).any()
        self.seats = updated_seats
        return state_changed


def read_input(input_filename):
    seats = []
    with open(input_filename) as f:
        for l in f:
            seats.append([c for c in l.strip()])
    return np.array(seats)


def solve_part1(seats):
    f = Ferry(seats)
    state_changed = True
    while state_changed:
        state_changed = f.step()
    return (f.seats == "#").sum()


def solve_part2(seats):
    f = Ferry(seats, mode=2)
    state_changed = True
    while state_changed:
        state_changed = f.step()
    return (f.seats == "#").sum()


def lines_to_array(lines):
    return np.array([[c for c in l] for l in lines])


example_input1 = [
    "L.LL.LL.LL",
    "LLLLLLL.LL",
    "L.L.L..L..",
    "LLLL.LL.LL",
    "L.LL.LL.LL",
    "L.LLLLL.LL",
    "..L.L.....",
    "LLLLLLLLLL",
    "L.LLLLLL.L",
    "L.LLLLL.LL",
]


def test_part1():
    assert solve_part1(lines_to_array(example_input1)) == 37


example_sight1 = [
    ".......#.",
    "...#.....",
    ".#.......",
    ".........",
    "..#L....#",
    "....#....",
    ".........",
    "#........",
    "...#.....",
]

example_sight2 = [
    ".............",
    ".L.L.#.#.#.#.",
    ".............",
]

example_sight3 = [
    ".##.##.",
    "#.#.#.#",
    "##...##",
    "...L...",
    "##...##",
    "#.#.#.#",
    ".##.##.",
]


@pytest.mark.parametrize(
    "test_input,pos,expected",
    [
        (example_sight1, (4, 3), 8),
        (example_sight2, (1, 1), 0),
        (example_sight3, (3, 3), 0),
    ],
)
def test_n_in_sight(test_input, pos, expected):
    f = Ferry(lines_to_array(test_input), mode=2)
    assert f.n_occupied_in_sight(*pos) == expected


def test_part2():
    assert solve_part2(lines_to_array(example_input1)) == 26


if __name__ == "__main__":

    seats = read_input("input")
    print("Part1:", solve_part1(seats))
    print("Part2:", solve_part2(seats))
