#!/usr/bin/env python


class Ship:

    directions = ("N", "E", "S", "W")

    def __init__(
        self,
        start_pos=(0, 0),
        start_direction="E",
        move_relative_to_waypoint=False,
        initial_waypoint=None,
    ):
        self.direction_index = self.directions.index(start_direction)
        self.pos = start_pos
        self.move_relative_to_waypoint = move_relative_to_waypoint
        self.waypoint = initial_waypoint

    @staticmethod
    def get_updated_pos(pos, direction, value):
        if direction == "N":
            pos = (pos[0], pos[1] + value)
        elif direction == "S":
            pos = (pos[0], pos[1] - value)
        elif direction == "E":
            pos = (pos[0] + value, pos[1])
        elif direction == "W":
            pos = (pos[0] - value, pos[1])
        return pos

    def update_pos(self, direction, value):
        self.pos = self.get_updated_pos(self.pos, direction, value)

    def update_waypoint(self, direction, value):
        self.waypoint = self.get_updated_pos(self.waypoint, direction, value)

    def turn_ship(self, value):
        value = value // 90
        self.direction_index = (self.direction_index + value) % len(self.directions)

    def turn_waypoint(self, value):
        if value < 0:
            value = 360 + value
        if value == 90:
            self.waypoint = (self.waypoint[1], -self.waypoint[0])
        elif value == 180:
            self.waypoint = (-self.waypoint[0], -self.waypoint[1])
        elif value == 270:
            self.waypoint = (-self.waypoint[1], self.waypoint[0])
        else:
            raise NotImplementedError(f"can't turn waypoint by {value} degrees")

    def turn(self, value):
        if self.move_relative_to_waypoint:
            self.turn_waypoint(value)
        else:
            self.turn_ship(value)

    def step(self, instruction, value):
        if instruction in ("N", "S", "E", "W"):
            if self.move_relative_to_waypoint:
                self.update_waypoint(instruction, value)
            else:
                self.update_pos(instruction, value)
        elif instruction == "L":
            self.turn(-value)
        elif instruction == "R":
            self.turn(value)
        elif instruction == "F":
            if self.move_relative_to_waypoint:
                self.pos = (
                    self.pos[0] + value * self.waypoint[0],
                    self.pos[1] + value * self.waypoint[1],
                )
            else:
                self.update_pos(self.directions[self.direction_index], value)


def parse_instruction(instruction_str):
    instruction = instruction_str[0]
    value = int(instruction_str[1:])
    return instruction, value


def solve_part1(instructions):
    ship = Ship()
    for instruction, value in instructions:
        ship.step(instruction, value)
    return abs(ship.pos[0]) + abs(ship.pos[1])


def solve_part2(instructions):
    ship = Ship(move_relative_to_waypoint=True, initial_waypoint=(10, 1))
    for instruction, value in instructions:
        ship.step(instruction, value)
    return abs(ship.pos[0]) + abs(ship.pos[1])


instruction_strings_example1 = [
    "F10",
    "N3",
    "F7",
    "R90",
    "F11",
]


def test_part1():
    instructions = [parse_instruction(s) for s in instruction_strings_example1]
    assert solve_part1(instructions) == 25


def test_part2():
    instructions = [parse_instruction(s) for s in instruction_strings_example1]
    assert solve_part2(instructions) == 286


if __name__ == "__main__":

    instructions = []
    with open("input") as f:
        for l in f:
            s = l.strip()
            instruction = s[0]
            value = int(s[1:])
            instructions.append((instruction, value))

    print(solve_part1(instructions))
    print(solve_part2(instructions))
