#!/usr/bin/env python

import numpy as np
import math


def parse_bus_ids(bus_id_line):
    return np.array([int(bus_id) for bus_id in bus_id_line.split(",") if bus_id != "x"])


def solve_part1(time, bus_ids):
    waiting_times = bus_ids - np.mod(time, bus_ids)
    list_id = np.argmin(waiting_times)
    return bus_ids[list_id] * waiting_times[list_id]


time_example1 = 939
bus_ids_example1 = "7,13,x,x,59,x,31,19"


def test_example1():
    assert solve_part1(time_example1, parse_bus_ids(bus_ids_example1)) == 295


def find_offset_and_period(a, b, time_shift, initial_offset, initial_period):
    t = initial_offset + time_shift
    offset = None
    period = None
    while True:
        if (t % b == 0) and (t % a == time_shift):
            if offset is None:
                offset = t
            else:
                return offset, t - offset
        t += initial_period


def parse_bus_ids_and_time_shifts(bus_id_line):
    # time shifts relative to previous
    bus_ids = []
    time_shifts = []
    for time_shift, bus_id in enumerate(bus_id_line.split(",")):
        if bus_id == "x":
            continue
        bus_ids.append(int(bus_id))
        time_shifts.append(time_shift)
    return bus_ids, np.diff(time_shifts)


def solve_part2(bus_ids, time_shifts):
    offset = 0
    period = 1
    a = bus_ids[0]
    for bus_id, time_shift in zip(bus_ids[1:], time_shifts):
        b = bus_id
        offset, period = find_offset_and_period(a, b, time_shift, offset, period)
        a = b
    return offset - sum(time_shifts)


def test_example1_part2():
    bus_ids, time_shifts = parse_bus_ids_and_time_shifts(bus_ids_example1)
    assert solve_part2(bus_ids, time_shifts) == 1068781


if __name__ == "__main__":

    with open("input") as f:
        content = f.read()
        time, bus_ids, _ = content.split("\n")
        time = int(time)
        bus_ids, time_shifts = parse_bus_ids_and_time_shifts(bus_ids)

    print(solve_part1(time, bus_ids))
    print(solve_part2(bus_ids, time_shifts))
