#!/usr/bin/env python


def set_bit(i, k):
    return i | (1 << k)


def unset_bit(i, k):
    return i & ~(1 << k)


class Computer:
    def __init__(self, version=1):
        self.memory = {}
        self._mask = {}
        self.version = version

    @property
    def mask(self):
        return self._mask

    @mask.setter
    def mask(self, mask_str):
        self._mask.clear()
        for i, c in enumerate(mask_str[::-1]):
            if c != "X":
                self._mask[i] = int(c)
            else:
                self._mask[i] = "X"

    def apply_mask_v1(self, key):
        for k, v in self._mask.items():
            if self.version == 1:
                if v == 1:
                    self.memory[key] = set_bit(self.memory[key], k)
                elif v == 0:
                    self.memory[key] = unset_bit(self.memory[key], k)

    def __setitem__(self, key, value):
        if self.version == 1:
            self.memory[key] = value
            self.apply_mask_v1(key)
        else:
            n_addr = 2 ** len([v for v in self.mask.values() if v == "X"])
            float_bit_indices = {
                i_bit: i_float
                for i_float, i_bit in enumerate(
                    sorted([k for k, v in self.mask.items() if v == "X"])
                )
            }
            for i_float in range(n_addr):
                new_key = key
                for i_bit, v_mask in self._mask.items():
                    if v_mask == 1:
                        new_key = set_bit(new_key, i_bit)
                    if v_mask == "X":
                        if (i_float >> float_bit_indices[i_bit]) & 1:
                            new_key = set_bit(new_key, i_bit)
                        else:
                            new_key = unset_bit(new_key, i_bit)
                self.memory[new_key] = value

    def parse_instruction(self, instruction):
        if "mask" in instruction:
            mask = instruction.strip().split()[2]
            self.mask = mask
        else:
            k = int(instruction.strip().split("[")[1].split("]")[0])
            v = int(instruction.strip().split()[2])
            self[k] = v


def test_example1_part1():
    computer = Computer()
    computer.parse_instruction("mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X")
    computer.parse_instruction("mem[8] = 11")
    computer.parse_instruction("mem[7] = 101")
    computer.parse_instruction("mem[8] = 0")
    assert sum(computer.memory.values()) == 165


def test_example2_part2():
    computer = Computer(version=2)
    computer.parse_instruction("mask = 000000000000000000000000000000X1001X")
    computer.parse_instruction("mem[42] = 100")
    computer.parse_instruction("mask = 00000000000000000000000000000000X0XX")
    computer.parse_instruction("mem[26] = 1")
    assert sum(computer.memory.values()) == 208


if __name__ == "__main__":

    computer = Computer()
    with open("input") as f:
        for l in f:
            computer.parse_instruction(l)
    print("Part1:", sum(computer.memory.values()))

    computer = Computer(version=2)
    with open("input") as f:
        for l in f:
            computer.parse_instruction(l)
    print("Part2:", sum(computer.memory.values()))
