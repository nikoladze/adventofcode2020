#!/usr/bin/env python

starting_numbers = [1,20,8,12,0,14]

def generate_numbers(starting_numbers):
    spoken_time = {}
    last_number = None
    i = 0
    while True:
        if i < len(starting_numbers):
            last_number = starting_numbers[i]
        elif len(spoken_time[last_number]) == 1:
            last_number = 0
        else:
            last_number = spoken_time[last_number][-1] - spoken_time[last_number][-2]
        yield last_number
        if not last_number in spoken_time:
            spoken_time[last_number] = []
        spoken_time[last_number].append(i)
        i += 1

if __name__ == "__main__":

    g = generate_numbers(starting_numbers)
    for i, n in zip(range(2020), g):
        pass
    print("Part1:", n)

    from tqdm import tqdm
    g = generate_numbers(starting_numbers)
    total = 30000000
    for i, n in tqdm(zip(range(total), g), total=total):
        pass
    print("Part2:", n)
