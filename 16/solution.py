#!/usr/bin/env python


from operator import mul
from functools import reduce


def parse_ticket_rule(line):
    field, rules = line.strip().split(":")
    rules = [[int(i) for i in rule.strip().split("-")] for rule in rules.split("or")]
    return field, rules


example_notes = """class: 1-3 or 5-7
row: 6-11 or 33-44
seat: 13-40 or 45-50

your ticket:
7,1,14

nearby tickets:
7,3,47
40,4,50
55,2,20
38,6,12
"""


def parse_notes(notes):
    field_ranges = {}
    in_block = "rule"
    ticket = None
    nearby_tickets = []
    for l in notes.split("\n"):
        if len(l.strip()) == 0:
            continue
        if "your ticket" in l:
            in_block = "ticket"
            continue
        if "nearby ticket" in l:
            in_block = "nearby"
            continue
        if in_block == "rule":
            field, rules = parse_ticket_rule(l)
            field_ranges[field] = rules
        else:
            ticket_numbers = [int(i) for i in l.split(",")]
            if in_block == "ticket":
                ticket = ticket_numbers
            elif in_block == "nearby":
                nearby_tickets.append(ticket_numbers)
            else:
                raise Exception("Something went wrong")
    return ticket, nearby_tickets, field_ranges


def valid_number(number, field_ranges):
    return any(
        [
            any([number in range(start, stop + 1) for start, stop in ranges])
            for ranges in field_ranges.values()
        ]
    )


def valid_options(number, field_ranges):
    options = []
    for field, ranges in field_ranges.items():
        for start, stop in ranges:
            if number in range(start, stop + 1):
                options.append(field)
    return set(options)


def solve_part1(nearby_tickets, field_ranges):
    error_rate = 0
    for ticket in nearby_tickets:
        for number in ticket:
            if not valid_number(number, field_ranges):
                error_rate += number
    return error_rate


def test_example1():
    my_ticket, nearby_tickets, field_ranges = parse_notes(example_notes)
    assert solve_part1(nearby_tickets, field_ranges) == 71


def find_single_option(cleared, options_list):
    for i, options in enumerate(options_list):
        if len(options) == 1 and not list(options)[0] in cleared:
            return options, i


def find_unique_options(field_ranges, nearby_tickets):
    options_list = [set() for i in nearby_tickets[0]]
    for ticket in nearby_tickets:
        for i, number in enumerate(ticket):
            new_options = valid_options(number, field_ranges)
            if len(new_options) == 0:
                continue
            if len(options_list[i]) == 0:
                options_list[i] = new_options
            options_list[i] = options_list[i].intersection(new_options)

    cleared = set()
    while True:
        single_option, single_option_index = find_single_option(cleared, options_list)
        for i, option in enumerate(options_list):
            if i == single_option_index:
                continue
            options_list[i] = options_list[i].difference(single_option)
        cleared.add(list(single_option)[0])
        if all([len(options) == 1 for options in options_list]):
            break

    return [options.pop() for options in options_list]


def solve_part2(my_ticket, nearby_tickets, field_ranges):
    unique_options = find_unique_options(field_ranges, nearby_tickets)
    return reduce(
        mul,
        [
            number
            for field, number in zip(unique_options, my_ticket)
            if field.startswith("departure")
        ],
    )


if __name__ == "__main__":

    my_ticket, nearby_tickets, field_ranges = parse_notes(example_notes)
    print(find_unique_options(field_ranges, nearby_tickets))

    with open("input") as f:
        my_ticket, nearby_tickets, field_ranges = parse_notes(f.read())

    print(solve_part1(nearby_tickets, field_ranges))
    print(solve_part2(my_ticket, nearby_tickets, field_ranges))
