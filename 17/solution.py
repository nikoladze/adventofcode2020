#!/usr/bin/env python


class ConwayCubes4D:
    def __init__(self, input_lines, restrict_to_3D=False):
        self.restrict_to_3D = restrict_to_3D
        self.active = set()
        for y, line in enumerate(input_lines):
            for x, c in enumerate(line):
                if c == "#":
                    self.active.add((x, y, 0, 0))

    def __repr__(self):
        output = []
        x_coords, y_coords, z_coords, w_coords = zip(*self.active)
        for w in range(min(w_coords), max(w_coords) + 1):
            for z in range(min(z_coords), max(z_coords) + 1):
                output.append(f"z = {z}, w = {w}\n")
                for y in range(min(y_coords), max(y_coords) + 1):
                    for x in range(min(x_coords), max(x_coords) + 1):
                        if (x, y, z, w) in self.active:
                            output.append("#")
                        else:
                            output.append(".")
                    output.append("\n")
        return "".join(output)

    def count_adjacent(self, coordinate):
        cx, cy, cz, cw = coordinate
        count = 0
        if self.restrict_to_3D:
            w_range = range(0, 1)
        else:
            w_range = range(cw - 1, cw + 2)
        for w in w_range:
            for z in range(cz - 1, cz + 2):
                for y in range(cy - 1, cy + 2):
                    for x in range(cx - 1, cx + 2):
                        if (x, y, z, w) == coordinate:
                            continue
                        if (x, y, z, w) in self.active:
                            count += 1
        return count

    def step(self):
        new_active = set()
        x_coords, y_coords, z_coords, w_coords = zip(*self.active)
        if self.restrict_to_3D:
            w_range = range(0, 1)
        else:
            w_range = range(min(w_coords) - 1, max(w_coords) + 2)
        for w in w_range:
            for z in range(min(z_coords) - 1, max(z_coords) + 2):
                for y in range(min(y_coords) - 1, max(y_coords) + 2):
                    for x in range(min(x_coords) - 1, max(x_coords) + 2):
                        n_adjacent = self.count_adjacent((x, y, z, w))
                        if (x, y, z, w) in self.active:
                            if n_adjacent == 2 or n_adjacent == 3:
                                new_active.add((x, y, z, w))
                        else:
                            if n_adjacent == 3:
                                new_active.add((x, y, z, w))
        self.active = new_active


example_input = [
    ".#.",
    "..#",
    "###",
]


def solve(lines, restrict_to_3D=False):
    cubes = ConwayCubes4D(lines, restrict_to_3D=restrict_to_3D)
    for i in range(6):
        cubes.step()
    return len(cubes.active)


def test_part1():
    assert solve(example_input, restrict_to_3D=True) == 112


def test_part2():
    assert solve(example_input, restrict_to_3D=False) == 848


if __name__ == "__main__":

    with open("input") as f:
        lines = f.read().strip().split("\n")

    print("Part1:", solve(lines, restrict_to_3D=True))
    print("Part1:", solve(lines, restrict_to_3D=False))
