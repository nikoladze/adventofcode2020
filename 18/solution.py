#!/usr/bin/env python

import pytest
from operator import add, mul


class Calculator:
    def __init__(self, expr):
        self.expr = expr.strip().replace(" ", "")

    def eval(self, pos=0):
        left = None
        op = None
        while pos < len(self.expr):
            c = self.expr[pos]
            if c == ")":
                return left, pos
            if c == "+":
                op = add
                pos += 1
                continue
            if c == "*":
                op = mul
                pos += 1
                continue
            if c == "(":
                right, pos = self.eval(pos + 1)
            else:
                right = int(c)
            if op is not None:
                left = op(left, right)
            else:
                left = right
            pos += 1
        return left, pos


def calculate(expr):
    calc = Calculator(expr)
    return calc.eval()[0]


expr1 = "1 + 2 * 3 + 4 * 5 + 6"
expr2 = "1 + (2 * 3) + (4 * (5 + 6))"


def test_expr1():
    assert calculate(expr1) == 71


def test_expr2():
    assert calculate(expr2) == 51


@pytest.mark.parametrize(
    "test_input,expected",
    [
        (expr1, 71),
        (expr2, 51),
        ("2 * 3 + (4 * 5)", 26),
        ("5 + (8 * 3 + 9 + 3 * 4 * 3)", 437),
        ("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))", 12240),
        ("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2", 13632),
    ],
)
def test_examples(test_input, expected):
    assert calculate(test_input) == expected


def knut_parenthesize(expr):
    # https://en.wikipedia.org/wiki/Operator-precedence_parser#Alternative_methods
    expr = expr.strip().replace(" ", "")
    expr = (
        expr.replace("(", "(((")
        .replace(")", ")))")
        .replace("*", "))*((")
        .replace("+", ")+(")
    )
    return f"(({expr}))"


def calculate_advanced(expr):
    return calculate(knut_parenthesize(expr))


if __name__ == "__main__":

    with open("input") as f:
        print(sum([calculate(line) for line in f]))

    with open("input") as f:
        print(sum([calculate_advanced(line) for line in f]))
