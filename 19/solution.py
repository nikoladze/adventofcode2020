#!/usr/bin/env python

"""
Part 1 seems correct

Part 2 works on the example, but gives incorrect results
"""

import re

def parse_input(lines):
    rules = {}
    messages = []
    for line in lines:
        if ":" in line:
            rule_index, rule = line.strip().split(":")
            if not '"' in rule:
                #rule = f"({rule.strip()})"
                rule = rule.strip()
            else:
                rule = rule.replace('"', '').strip()
            rules[rule_index] = rule
            continue
        if len(line.strip()) == 0:
            continue
        messages.append(line.strip())
    return rules, messages


example_input1 = [
    '0: 4 1 5',
    '1: 2 3 | 3 2',
    '2: 4 4 | 5 5',
    '3: 4 5 | 5 4',
    '4: "a"',
    '5: "b"',
    '',
    'ababbb',
    'bababa',
    'abbbab',
    'aaabbb',
    'aaaabbb',
]

example_input2 = [
    '42: 9 14 | 10 1',
    '9: 14 27 | 1 26',
    '10: 23 14 | 28 1',
    '1: "a"',
    '11: 42 31',
    '5: 1 14 | 15 1',
    '19: 14 1 | 14 14',
    '12: 24 14 | 19 1',
    '16: 15 1 | 14 14',
    '31: 14 17 | 1 13',
    '6: 14 14 | 1 14',
    '2: 1 24 | 14 4',
    '0: 8 11',
    '13: 14 3 | 1 12',
    '15: 1 | 14',
    '17: 14 2 | 1 7',
    '23: 25 1 | 22 14',
    '28: 16 1',
    '4: 1 1',
    '20: 14 14 | 1 15',
    '3: 5 14 | 16 1',
    '27: 1 6 | 14 18',
    '14: "b"',
    '21: 14 1 | 1 14',
    '25: 1 1 | 1 14',
    '22: 14 14',
    '8: 42',
    '26: 14 22 | 1 20',
    '18: 15 15',
    '7: 14 5 | 1 21',
    '24: 14 1',
    '',
    'abbbbbabbbaaaababbaabbbbabababbbabbbbbbabaaaa',
    'bbabbbbaabaabba',
    'babbbbaabbbbbabbbbbbaabaaabaaa',
    'aaabbbbbbaaaabaababaabababbabaaabbababababaaa',
    'bbbbbbbaaaabbbbaaabbabaaa',
    'bbbababbbbaaaaaaaabbababaaababaabab',
    'ababaaaaaabaaab',
    'ababaaaaabbbaba',
    'baabbaaaabbaaaababbaababb',
    'abbbbabbbbaaaababbbbbbaaaababb',
    'aaaaabbaabaaaaababaa',
    'aaaabbaaaabbaaa',
    'aaaabbaabbaaaaaaabbbabbbaaabbaabaaa',
    'babaaabbbaaabaababbaabababaaab',
    'aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba',
]


def expand(rule, rules):
    "This seems to be a very bad idea ..."
    rule = f"({rule})"
    while True:
        if not re.match(".*[0-9].*", rule):
            break
        for token in rule.split():
            if not re.match(".*[0-9].*", token):
                continue
            number = re.sub("[^0-9]", "", token)
            rule = rule.replace(number, f" ({rules[number]}) ")
    return f"^{rule.replace(' ', '')}$"

def ab_to_int(abstring):
    number = 0
    for i, c in enumerate(abstring[::-1]):
        if c == "b":
            number += 2 ** i
    return number

DO_DEBUG = False

def debug(*msg):
    if DO_DEBUG:
        print(*msg)

def match(rules, pattern, message, pos=0, indent="", current_rule=None):
    if len(pattern.strip()) == 0:
        raise Exception("This should not happen right?")
    debug(indent, f"trying to match '{pattern}' for {message[pos:]}")
    if pos >= len(message):
        debug(indent, "Consumed the whole message, but still things left to match - so no match")
        return pos, False
    # try to match any of the patterns separated by "|"
    debug(indent, f"checking any of {pattern.split('|')}")
    for sub_pattern in pattern.split("|"):
        sub_pos = pos
        debug(indent, f"Checking sub_pattern '{sub_pattern}'")
        for c in sub_pattern.split():
            if c.isdigit():
                debug(indent, f"recurse '{c}' -> '{rules[c]}'")
                sub_pos, is_match = match(rules, rules[c], message, sub_pos, indent=indent+"    ", current_rule=c)
                debug(indent, f"{is_match}, next_part: {message[sub_pos:]}")
            else:
                is_match = message[pos] == c
                if is_match:
                    sub_pos += 1
            if not is_match:
                break
        else:
            # all of at least one option matched
            debug(indent, f"{sub_pattern} matched")
            debug(indent, f"next part: {message[sub_pos:]}")
            return sub_pos, True
    # None matched
    debug(indent, f"None matched")
    return pos, False

def solve_part1(rules, messages):
    count = 0
    for message in messages:
        pos, is_match = match(rules, rules['0'], message)
        if is_match and pos == len(message):
            count += 1
    return count

def match_weirdly(rules, message, pos=0):
    # first try 42
    is_match = True
    n_match_42 = 0
    while (pos < len(message)) and is_match:
        pos, is_match = match(rules, rules["42"], message, pos=pos)
        if is_match:
            n_match_42 += 1
    # now try 31
    is_match = True
    n_match_31 = 0
    while (pos < len(message)) and is_match:
        pos, is_match = match(rules, rules["31"], message, pos=pos)
        if is_match:
            n_match_31 += 1
    if (n_match_31 == 0) or (n_match_42 < 2) or (n_match_42 <= n_match_31):
        is_match = False
    return pos, is_match


def solve_part2(rules, messages):
    count = 0
    matching_messages = set()
    for message in messages:
        pos, is_match = match_weirdly(rules, message)
        if is_match and pos == len(message):
            matching_messages.add(message)
            count += 1
    return count, matching_messages

expected_matches_example2 = set([
    "bbabbbbaabaabba",
    "babbbbaabbbbbabbbbbbaabaaabaaa",
    "aaabbbbbbaaaabaababaabababbabaaabbababababaaa",
    "bbbbbbbaaaabbbbaaabbabaaa",
    "bbbababbbbaaaaaaaabbababaaababaabab",
    "ababaaaaaabaaab",
    "ababaaaaabbbaba",
    "baabbaaaabbaaaababbaababb",
    "abbbbabbbbaaaababbbbbbaaaababb",
    "aaaaabbaabaaaaababaa",
    "aaaabbaabbaaaaaaabbbabbbaaabbaabaaa",
    "aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba",
])

def test_example2():
    rules, messages = parse_input(example_input2)
    n_match, matching = solve_part2(rules, messages)
    assert set(matching) == expected_matches_example2
    assert n_match == 12


def test_my_examples():
    rules = {
        # "31" : "a a | b b",
        # "42" : "a b | b a",
        "31" : "1 1 | 2 2",
        "42" : "1 2 | 2 1",
        "1" : "a",
        "2" : "b",
    }
    # starts with 31
    assert match_weirdly(rules, "aabbab") == (4, False)
    # has only one 42 match
    assert match_weirdly(rules, "abaabb") == (6, False)
    # has no 31 match
    assert match_weirdly(rules, "ababbaba") == (8, False)
    # this one has exact same number of 31 and 42 matches
    assert match_weirdly(rules, "abbaaabb") == (8, False)
    # these should work
    assert match_weirdly(rules, "abbaabaa") == (8, True)
    assert match_weirdly(rules, "bababaabbaabaa") == (14, True)
    # these not
    assert match_weirdly(rules, "abbaaabba") == (8, False)
    assert match_weirdly(rules, "babbaaabb") == (6, False)


if __name__ == "__main__":

    with open("input") as f:
        lines = f.readlines()

    rules, messages = parse_input(lines)
    #rules, messages = parse_input(example_input2)
    #rules, messages = parse_input(example_input1)
    print("Part1:", solve_part1(rules, messages))

    #rules["8"] = "42 | 42 8"
    #rules["11"] = "42 31 | 42 11 31"

    n_match, matching = solve_part2(rules, messages)
    print("Part2:", n_match)
