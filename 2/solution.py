#!/usr/bin/env python


def get_valid_passwords_policy1():
    n_valid = 0
    with open("input") as f:
        for line in f:
            low, high, letter, password = (
                line.strip().replace(":", " ").replace("-", " ").split()
            )
            low, high = int(low), int(high)
            if low <= password.count(letter) <= high:
                n_valid += 1
    return n_valid


def get_valid_passwords_policy2():
    n_valid = 0
    with open("input") as f:
        for line in f:
            pos1, pos2, letter, password = (
                line.strip().replace(":", " ").replace("-", " ").split()
            )
            pos1, pos2 = [int(i) - 1 for i in [pos1, pos2]]
            if (password[pos1] == letter) ^ (password[pos2] == letter):
                n_valid += 1
    return n_valid


if __name__ == "__main__":
    print("Part1:", get_valid_passwords_policy1())
    print("Part2:", get_valid_passwords_policy2())
