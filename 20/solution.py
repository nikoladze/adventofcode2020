#!/usr/bin/env python

import numpy as np
from collections import defaultdict


def parse_input(string):
    tiles = {}
    for tile in string.split("\n\n"):
        if len(tile.strip()) == 0:
            continue
        lines = tile.strip().split("\n")
        tile_id = int(lines[0].split()[1].replace(":", ""))
        n = len(lines[1])
        tile_content = np.frombuffer(
            ("".join(lines[1:])).encode(), dtype=np.uint8
        ).reshape(n, n)
        tiles[tile_id] = tile_content
    return tiles


example_input = """
Tile 2311:
..##.#..#.
##..#.....
#...##..#.
####.#...#
##.##.###.
##...#.###
.#.#.#..##
..#....#..
###...#.#.
..###..###

Tile 1951:
#.##...##.
#.####...#
.....#..##
#...######
.##.#....#
.###.#####
###.##.##.
.###....#.
..#.#..#.#
#...##.#..

Tile 1171:
####...##.
#..##.#..#
##.#..#.#.
.###.####.
..###.####
.##....##.
.#...####.
#.##.####.
####..#...
.....##...

Tile 1427:
###.##.#..
.#..#.##..
.#.##.#..#
#.#.#.##.#
....#...##
...##..##.
...#.#####
.#.####.#.
..#..###.#
..##.#..#.

Tile 1489:
##.#.#....
..##...#..
.##..##...
..#...#...
#####...#.
#..#.#.#.#
...#.#.#..
##.#...##.
..##.##.##
###.##.#..

Tile 2473:
#....####.
#..#.##...
#.##..#...
######.#.#
.#...#.#.#
.#########
.###.#..#.
########.#
##...##.#.
..###.#.#.

Tile 2971:
..#.#....#
#...###...
#.#.###...
##.##..#..
.#####..##
.#..####.#
#..#.#..#.
..####.###
..#.#.###.
...#.#.#.#

Tile 2729:
...#.#.#.#
####.#....
..#.#.....
....#..#.#
.##..##.#.
.#.####...
####.#.#..
##.####...
##..#.##..
#.##...##.

Tile 3079:
#.#.#####.
.#..######
..#.......
######....
####.#..#.
.#...#.##.
#.#####.##
..#.###...
..#.......
..#.###...
"""


def tile_to_boolean(tile):
    return np.where(tile == ord("."), 0, np.where(tile == ord("#"), 1, 0))


# fmt: off
SEA_MONSTER = (
    "                  # "
    "#    ##    ##    ###"
    " #  #  #  #  #  #   "
)
# fmt: on

SEA_MONSTER_MASK = (
    tile_to_boolean(np.frombuffer(SEA_MONSTER.encode(), dtype=np.uint8).reshape(3, 20))
).astype(np.bool)


def array_to_bitmask(array):
    bitmask = 0
    for i, b in enumerate(array):
        bitmask += b << i
    return bitmask


def get_border(tile, pos):
    return tile[
        [
            # top, right, bottom, left
            # clockwise "pointing" vectors
            0,
            (slice(None), -1),
            (-1, slice(None, None, -1)),
            (slice(None, None, -1), 0),
        ][pos]
    ]


def get_border_dicts(tiles):
    # l, r, t, b, each normal and reversed
    border_tile_dict = defaultdict(list)
    tile_borders_dict = defaultdict(
        lambda: [[None, None, None, None], [None, None, None, None]]
    )
    for tile_id, tile in tiles.items():
        tile = tile_to_boolean(tile)
        for pos in range(4):
            array = get_border(tile, pos)
            key_normal = array_to_bitmask(array)
            key_reverse = array_to_bitmask(array[::-1])
            tile_borders_dict[tile_id][0][pos] = key_normal
            tile_borders_dict[tile_id][1][pos] = key_reverse
            for key in [key_normal, key_reverse]:
                border_tile_dict[key].append(tile_id)
    return border_tile_dict, tile_borders_dict


def find_corners(tiles):
    border_tile_dict, tile_borders_dict = get_border_dicts(tiles)
    corner_tiles = []
    for tile_id, borders in tile_borders_dict.items():
        missing = []
        # enough to look in the non-flipped borders
        for pos, border_key in enumerate(borders[0]):
            current_borders = border_tile_dict[border_key]
            if len(current_borders) == 1:
                missing.append(pos)
        if len(missing) == 2:
            corner_tiles.append((tile_id, missing))
    return corner_tiles


def solve_part1(input_string):
    tiles = parse_input(input_string)
    return np.prod([tile_id for tile_id, _ in find_corners(tiles)])


def rotate_corner(orientation):
    # rotate clockwise by 90 degrees
    return np.array(orientation)[[3, 0, 1, 2]]


def find_corner_rotation(missing, target_orientation=[0, 1, 1, 0]):
    current_orientation = []
    for i in range(4):
        if i in missing:
            current_orientation.append(0)
        else:
            current_orientation.append(1)
    for i in range(4):
        if (np.array(current_orientation) == np.array(target_orientation)).all():
            return i
        current_orientation = rotate_corner(current_orientation)


def rotate_tile(tile):
    return tile.T[:, ::-1]


def flip_y_tile(tile):
    return tile[::-1]


class Puzzle:
    def __init__(self, input_string):
        self.tiles = parse_input(input_string)
        self.border_tile_dict, self.tile_borders_dict = get_border_dicts(self.tiles)
        self.size = int(np.sqrt(len(self.tiles)))
        self.ordered_tiles = None

    def assert_same_borders_tile_dict(self, tile_id):
        tile = self.tiles[tile_id]
        for pos in range(4):
            border = tile_to_boolean(get_border(tile, pos))
            key = array_to_bitmask(border)
            key_reverse = array_to_bitmask(border[::-1])
            assert self.tile_borders_dict[tile_id][0][pos] == key
            assert self.tile_borders_dict[tile_id][1][pos] == key_reverse

    def permute_borders(self, tile_id, permutation):
        for i in range(2):
            border_ids = self.tile_borders_dict[tile_id][i]
            self.tile_borders_dict[tile_id][i] = [border_ids[j] for j in permutation]

    def rotate(self, tile_id):
        self.permute_borders(tile_id, [3, 0, 1, 2])
        self.tiles[tile_id] = rotate_tile(self.tiles[tile_id])
        self.assert_same_borders_tile_dict(tile_id)

    def flip_y(self, tile_id):
        # flip
        self.permute_borders(tile_id, [2, 1, 0, 3])
        # swap reverse <-> normal
        self.tile_borders_dict[tile_id] = [
            self.tile_borders_dict[tile_id][1],
            self.tile_borders_dict[tile_id][0],
        ]
        # transform tiles
        self.tiles[tile_id] = flip_y_tile(self.tiles[tile_id])
        self.assert_same_borders_tile_dict(tile_id)

    def flip_x(self, tile_id):
        # equivalent to flip_y and 180 degree rotation
        self.flip_y(tile_id)
        for i in range(2):
            self.rotate(tile_id)
        self.assert_same_borders_tile_dict(tile_id)

    def transform_tile_to_fit(self, tile_id, pos, border):
        # try all 3 rotations (use 4 because we check before rotating)
        for i in range(4):
            if self.tile_borders_dict[tile_id][1][pos] == border:
                # the second tile always has the borders in other direction, so
                # the orientation is correct if the reverse border matches
                # ^ ->  ^ ->
                # |  |  |  |
                # <- v  <- v
                return
            if self.tile_borders_dict[tile_id][0][pos] == border:
                # otherwise i need to flip it
                if pos in [1, 3]:
                    self.flip_y(tile_id)
                if pos in [0, 2]:
                    self.flip_x(tile_id)
                return
            self.rotate(tile_id)
        raise Exception(f"Can't fit tile {tile_id} to border {border} at pos {pos}")

    def assert_match(self, tile_id1, tile_id2, pos1, pos2):
        assert (
            get_border(self.tiles[tile_id1], pos1)
            == get_border(self.tiles[tile_id2], pos2)[::-1]
        ).all()

    def solve(self):
        ordered_tiles = []
        corners = find_corners(self.tiles)
        top_left_id, top_left_missing = corners[0]
        for i in range(find_corner_rotation(top_left_missing)):
            self.rotate(top_left_id)
        for j in range(self.size):
            if j == 0:
                # attach to top-left corner
                previous_id = top_left_id
            else:
                # attach to top row
                top_id = ordered_tiles[-1][0]
                bottom_border = self.tile_borders_dict[top_id][0][2]
                for previous_id in self.border_tile_dict[bottom_border]:
                    if previous_id != top_id:
                        break
                else:
                    raise Exception("Can't find first tile in this row")
                self.transform_tile_to_fit(previous_id, 0, bottom_border)
                self.assert_match(top_id, previous_id, 2, 0)
            row = []
            row.append(previous_id)
            for i in range(self.size - 1):
                right_border = self.tile_borders_dict[previous_id][0][1]
                for next_id in self.border_tile_dict[right_border]:
                    if next_id != previous_id:
                        break
                else:
                    raise Exception("Can't find next tile")
                # next tile should fit at position 3 (left)
                self.transform_tile_to_fit(next_id, 3, right_border)
                self.assert_match(previous_id, next_id, 1, 3)
                previous_id = next_id
                row.append(next_id)
            ordered_tiles.append(row)
        return ordered_tiles

    def solution_to_img(self, solution):
        return np.concatenate(
            [
                np.concatenate(
                    [tile_to_boolean(self.tiles[k])[1:-1, 1:-1] for k in row], axis=1
                )
                for row in solution
            ],
            axis=0,
        )


def find_seamonsters(img):
    w = SEA_MONSTER_MASK.shape[0]
    h = SEA_MONSTER_MASK.shape[1]
    found = False
    positions = []
    for i in range(img.shape[0] - w):
        for j in range(img.shape[1] - h):
            if img[i : i + w, j : j + h][SEA_MONSTER_MASK].all():
                positions.append((i, j))
                found = True
    return found, positions


def find_orientation_with_seamonsters(img):
    img = np.array(img)
    for i in range(2):
        for j in range(4):
            found, positions = find_seamonsters(img)
            if found:
                trf = (i, j)
                return img, positions
            img = rotate_tile(img)
        img = flip_y_tile(img)


def mark_seamonsters(img, positions):
    w = SEA_MONSTER_MASK.shape[0]
    h = SEA_MONSTER_MASK.shape[1]
    img_marked = np.array(img)
    for pos in positions:
        img_marked[pos[0] : pos[0] + w, pos[1] : pos[1] + h][SEA_MONSTER_MASK] = 2
    return img_marked

def solve_part2(input_string):
    puzzle = Puzzle(input_string)
    img = puzzle.solution_to_img(puzzle.solve())
    correct_img, seamonster_positions = find_orientation_with_seamonsters(img)
    marked_img = mark_seamonsters(correct_img, seamonster_positions)
    return (marked_img == 1).sum()


def validate_solution(tile_array):
    # check left/right borders
    assert (tile_array[:, :-1, :, -1] == tile_array[:, 1:, :, 0]).all()
    # check top/bottom borders
    assert (tile_array[:-1, :, -1, :] == tile_array[1:, :, 0, :]).all()


def test_solution():
    puzzle = Puzzle(example_input)
    solution = puzzle.solve()
    tile_array = np.array([[puzzle.tiles[k] for k in row] for row in solution])
    validate_solution(tile_array)


if __name__ == "__main__":

    with open("input") as f:
        input_string = f.read()

    print("Part1:", solve_part1(input_string))
    print("Part2:", solve_part2(input_string))
