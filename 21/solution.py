#!/usr/bin/env python

from functools import reduce

example_input1 = [
    "mxmxvkd kfcds sqjhc nhms (contains dairy, fish)",
    "trh fvjkl sbzzf mxmxvkd (contains dairy)",
    "sqjhc fvjkl (contains soy)",
    "sqjhc mxmxvkd sbzzf (contains fish)",
]


def parse_input(lines):
    ingredients_allergens = []
    for line in lines:
        ingredients, allergens = line.strip().split("(")
        ingredients = ingredients.strip().split()
        allergens = (
            allergens.replace("contains", "").replace(",", "").replace(")", "").split()
        )
        ingredients_allergens.append((set(ingredients), set(allergens)))
    return ingredients_allergens


def eliminate(allergen, ingredient, ingredients_allergens):
    for ingredients, allergens in ingredients_allergens:
        ingredients.discard(ingredient)
        allergens.discard(allergen)


def solve(lines):
    ingredients_allergens = parse_input(lines)
    found = {}
    while True:
        all_allergens = set(sum([list(x[1]) for x in ingredients_allergens], []))
        if len(all_allergens) == 0:
            break
        for allergen in all_allergens:
            # intersection of all ingredients of lists that contain this allergen
            intersection = reduce(
                set.intersection,
                [x[0] for x in ingredients_allergens if allergen in x[1]],
            )
            if len(intersection) == 1:
                ingredient = list(intersection)[0]
                found[allergen] = ingredient
                eliminate(allergen, ingredient, ingredients_allergens)
                break
    return found, ingredients_allergens


def solve_part1(lines):
    found, rest = solve(lines)
    return len(sum([list(r[0]) for r in rest], []))


def solve_part2(lines):
    found, rest = solve(lines)
    return ",".join([v for k, v in sorted(found.items(), key=lambda x: x[0])])


def test_part1():
    assert solve_part1(example_input1) == 5


def test_part1():
    assert solve_part2(example_input1) == "mxmxvkd,sqjhc,fvjkl"


if __name__ == "__main__":
    with open("input") as f:
        lines = f.readlines()

    print("Part1:", solve_part1(lines))
    print("Part2:", solve_part2(lines))
