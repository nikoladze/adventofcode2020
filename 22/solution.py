#!/usr/bin/env python

from collections import deque
import numpy as np


def parse_input(input_string):
    card_decks = []
    for block in input_string.strip().split("\n\n"):
        card_decks.append(deque([int(i) for i in block.split("\n")[1:]]))
    for deck in card_decks:
        deck.reverse()
    return card_decks


example_input = """
Player 1:
9
2
6
3
1

Player 2:
5
8
4
7
10
"""

example_input_infinite = """
Player 1:
43
19

Player 2:
2
29
14
"""

DEBUG = False


def debug(*args, **kwargs):
    if DEBUG:
        print(*args, **kwargs)


def play_combat(card_decks):
    # copy
    card_decks = [deque(deck) for deck in card_decks]
    round = 1
    while len(card_decks[0]) > 0 and len(card_decks[1]) > 0:
        debug(f"-- Round {round} --")
        debug(f"Player 1's deck: {card_decks[0]}")
        debug(f"Player 2's deck: {card_decks[1]}")
        card0 = card_decks[0].pop()
        card1 = card_decks[1].pop()
        debug(f"Player 1 plays: {card0}")
        debug(f"Player 2 plays: {card1}")
        if card0 > card1:
            debug(f"Player 1 wins the round!")
            card_decks[0].extendleft([card0, card1])
        else:
            debug(f"Player 2 wins the round!")
            card_decks[1].extendleft([card1, card0])
        debug("")
        round += 1
    debug("")
    debug("== Post-game results ==")
    debug(f"Player 1's deck: {card_decks[0]}")
    debug(f"Player 2's deck: {card_decks[1]}")
    if len(card_decks[0]) == 0:
        return card_decks[1]
    else:
        return card_decks[0]


def play_recursive_combat(card_decks, game=1):
    # copy
    next_game = game + 1
    card_decks = [deque(deck) for deck in card_decks]
    memory0 = set()
    memory1 = set()
    winner = None
    #debug(f"\n=== Game {game} ===")
    round = 1
    game_situations = []
    while True:
        #debug(f"\n-- Round {round} (Game {game}) --")
        #debug(f"Player 1's deck: {card_decks[0]}")
        #debug(f"Player 2's deck: {card_decks[1]}")
        if len(card_decks[0]) == 0:
            winner = 1
            break
        if len(card_decks[1]) == 0:
            winner = 0
            break
        if tuple(card_decks[0]) in memory0 or tuple(card_decks[1]) in memory1:
            #debug(f"Been there before ... we won't play forever - Player 1 wins!")
            winner = 0
            break
        memory0.add(tuple(card_decks[0]))
        memory1.add(tuple(card_decks[1]))
        card0 = card_decks[0].pop()
        card1 = card_decks[1].pop()
        #debug(f"Player 1 plays: {card0}")
        #debug(f"Player 2 plays: {card1}")
        if len(card_decks[0]) >= card0 and len(card_decks[1]) >= card1:
            #debug("\nPlaying a sub-game to determine the winner...\n")
            round_winner, _ = play_recursive_combat(
                (list(card_decks[0])[card0:], list(card_decks[1])[card1:]),
                game=next_game,
            )
            next_game += 1
        else:
            round_winner = card1 > card0
        #debug(f"Player {round_winner + 1} wins round {round} of game {game}!")
        if round_winner == 0:
            card_decks[0].extendleft([card0, card1])
        else:
            card_decks[1].extendleft([card1, card0])
        round += 1
    if game > 1:
        #debug(f"\n...anyway, back to game {game - 1}.\n")
        pass
    result = winner, card_decks[winner]
    return result


def solve_part1(input_string):
    res = play_combat(parse_input(input_string))
    return (np.arange(1, len(res) + 1) * res).sum()


def solve_part2(input_string):
    winner, res = play_recursive_combat(parse_input(input_string))
    return (np.arange(1, len(res) + 1) * res).sum()


if __name__ == "__main__":

    with open("input") as f:
        input_string = f.read()

    print("Part1:", solve_part1(input_string))
    print("Part2:", solve_part2(input_string))
