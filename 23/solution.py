#!/usr/bin/env python

import logging
from collections import deque
from tqdm import tqdm

logger = logging.getLogger("day23")
debug = logger.debug
info = logger.info

example_input = "389125467"
puzzle_input = "784235916"


class Cups:
    def __init__(self, input_string, extended=False, extend_until=1000000):
        values = [int(i) for i in input_string]
        self.extended = extended
        if extended:
            values.extend(range(max(values) + 1, extend_until + 1))
        self.links = dict(
            (c, (p, n)) for p, c, n in zip(values[:-2], values[1:-1], values[2:])
        )
        self.links[values[0]] = (values[-1], values[1])
        self.links[values[-1]] = (values[-2], values[0])
        self.destination = values[0]
        self.current = values[0]
        self.low = min(values)
        self.high = max(values)

    def remove(self, value):
        left, right = self.links.pop(value)
        self.links[left] = (self.links[left][0], right)
        self.links[right] = (left, self.links[right][1])

    def pickup(self):
        picked_cups = []
        value = self.current
        for i in range(3):
            # pick the one right of current value
            value = self.links[value][1]
            picked_cups.append(value)
        for value in picked_cups:
            self.remove(value)
        return picked_cups

    def update_destination(self, picked_cups):
        destination = self.current
        low, high = self.low, self.high
        # total length should be an upper bound
        for i in range(len(self.links)):
            destination -= 1
            destination = low + (destination - low) % (high + 1 - low)
            if not destination in picked_cups:
                self.destination = destination
                return
        raise Exception("Can't find next destination")

    def insert_after(self, place_after, value):
        right = self.links[place_after][1]
        self.links[value] = (place_after, right)
        self.links[right] = (value, self.links[right][1])
        self.links[place_after] = (self.links[place_after][0], value)

    def place_cups(self, picked_cups):
        place_after = self.destination
        for cup in picked_cups:
            self.insert_after(place_after, cup)
            place_after = cup

    def update_current(self):
        self.current = self.links[self.current][1]

    def __repr__(self):
        values = [self.current]
        for i in range(len(self.links) - 1):
            values.append(self.links[values[-1]][1])
        return " ".join([str(v) for v in values])


def play(input_string, moves=100, **kwargs):
    cups = Cups(input_string, **kwargs)
    move = 1
    for i in tqdm(range(moves)):
        # info("")
        # info(f"-- move {move} --")
        # info(f"cups: {cups}")
        pickup = cups.pickup()
        # info(f"pickup: {pickup}")
        cups.update_destination(pickup)
        # info(f"destination: {cups.destination}")
        cups.place_cups(pickup)
        cups.update_current()
        # cups.rotate(-1)
        move += 1
    # info("")
    # info("-- final --")
    # info(f"cups: {cups}")
    # cups.rotate(-cups.index(1))
    # if not extended:
    #     return "".join(str(i) for i in list(cups)[1:])
    cups.current = 1
    # return "".join(str(i) for i in list(cups)[1:])
    return cups


def solve_part1(input_string):
    cups = play(input_string)
    return cups.__repr__().replace(" ", "")[1:]


def solve_part2(input_string):
    cups = play(puzzle_input, extended=True, moves=10000000, extend_until=1000000)
    return cups.links[1][1] * cups.links[cups.links[1][1]][1]


if __name__ == "__main__":

    # logging.basicConfig()
    # logger.setLevel(logging.INFO)

    print("Part1:", solve_part1(puzzle_input))
    print("Part2:", solve_part2(puzzle_input))
