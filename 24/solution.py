#!/usr/bin/env python

import re
from collections import defaultdict

example_input = [
    "sesenwnenenewseeswwswswwnenewsewsw",
    "neeenesenwnwwswnenewnwwsewnenwseswesw",
    "seswneswswsenwwnwse",
    "nwnwneseeswswnenewneswwnewseswneseene",
    "swweswneswnenwsewnwneneseenw",
    "eesenwseswswnenwswnwnwsewwnwsene",
    "sewnenenenesenwsewnenwwwse",
    "wenwwweseeeweswwwnwwe",
    "wsweesenenewnwwnwsenewsenwwsesesenwne",
    "neeswseenwwswnwswswnw",
    "nenwswwsewswnenenewsenwsenwnesesenew",
    "enewnwewneswsewnwswenweswnenwsenwsw",
    "sweneswneswneneenwnewenewwneswswnese",
    "swwesenesewenwneswnwwneseswwne",
    "enesenwswwswneneswsenwnewswseenwsese",
    "wnwnesenesenenwwnenwsewesewsesesew",
    "nenewswnwewswnenesenwnesewesw",
    "eneswnwswnwsenenwnwnwwseeswneewsenese",
    "neswnwewnwnwseenwseesewsenwsweewe",
    "wseweeenwnesenwwwswnew",
]

relative_coords = {
    "e": (2, 0),
    "se": (1, -2),
    "sw": (-1, -2),
    "w": (-2, 0),
    "nw": (-1, 2),
    "ne": (1, 2),
}


def parse_input(lines):
    tokens_list = []
    for line in lines:
        tokens_list.append(
            [
                token
                for token in re.split("(e|se|sw|w|nw|ne)", line.strip())
                if len(token) != 0
            ]
        )
    return tokens_list


def find_tile(tokens, start=(0, 0)):
    pos = start
    for token in tokens:
        pos_rel = relative_coords[token]
        pos = (pos[0] + pos_rel[0], pos[1] + pos_rel[1])
    return pos


def get_tile_dict(lines):
    tokens_list = parse_input(lines)
    flipped = defaultdict(int)
    for tokens in tokens_list:
        pos = find_tile(tokens)
        flipped[pos] = not flipped[pos]
    return flipped

def get_n_adjacent_tiles(tile_dict, pos):
    n = 0
    for rel_pos in relative_coords.values():
        if tile_dict[(pos[0] + rel_pos[0], pos[1] + rel_pos[1])]:
            n += 1
    return n

def apply_rules(tile_dict, pos):
    n_adjacent = get_n_adjacent_tiles(tile_dict, pos)
    flipped = tile_dict[pos]
    if (
        (flipped and not ((n_adjacent > 2) or (n_adjacent == 0)))
        or (not flipped and n_adjacent == 2)
    ):
        return True
    return False

def evolve(tile_dict):
    new_tiles = defaultdict(int)
    already_checked = set()

    def update(pos):
        if pos in already_checked:
            return
        if apply_rules(tile_dict, pos):
            new_tiles[pos] = True
        already_checked.add(pos)

    for pos, flipped in list(tile_dict.items()):
        update(pos)
        # also check adjacent tiles
        for rel_pos in relative_coords.values():
            update((pos[0] + rel_pos[0], pos[1] + rel_pos[1]))

    return new_tiles


def solve_part1(lines):
    return sum(get_tile_dict(lines).values())

def solve_part2(lines):
    tile_dict = get_tile_dict(lines)
    for i in range(100):
        tile_dict = evolve(tile_dict)
    return sum(tile_dict.values())


def test_example_part1():
    assert solve_part1(example_input) == 10


if __name__ == "__main__":

    with open("input") as f:
        lines = f.read().strip().split("\n")

    print(solve_part1(lines))
    print(solve_part2(lines))
