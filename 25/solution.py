#!/usr/bin/env python

MOD = 20201227


def find_loop_size(public_key, subject_number=7):
    n = 1
    i = 1
    while True:
        n *= subject_number
        n %= MOD
        if n == public_key:
            return i
        i += 1


def get_encryption_key(loop_size, other_public_key):
    enc = 1
    for i in range(loop_size):
        enc *= other_public_key
        enc %= MOD
    return enc


def solve_part1(pubkey1, pubkey2):
    loop1, loop2 = map(find_loop_size, [pubkey1, pubkey2])
    # we could save time by only running the one with the smaller loop size,
    # but seems to be fine still ;)
    enc1, enc2 = [
        get_encryption_key(loop, pubkey)
        for loop, pubkey in [(loop2, pubkey1), (loop1, pubkey2)]
    ]
    assert enc1 == enc2
    return enc1


if __name__ == "__main__":

    with open("input") as f:
        pubkey1, pubkey2 = map(int, f.read().strip().split("\n"))

    print(solve_part1(pubkey1, pubkey2))
