#!/usr/bin/env python

def get_tree_map():
    tree_map = []
    with open("input") as f:
        for line in f:
            tree_map.append(line.strip())
    return tree_map

def test_slope(tree_map, slope):
    pos = (0, 0)
    n_hits = 0
    while pos[1] < len(tree_map):
        tree_row = tree_map[pos[1]]
        if tree_row[pos[0] % len(tree_row)] == "#":
            n_hits +=1
        pos = (pos[0] + slope[0], pos[1] + slope[1])
    return n_hits

if __name__ == "__main__":

    from operator import mul
    from functools import reduce

    tree_map = get_tree_map()

    print("Part1: ", test_slope(tree_map, (3, 1)))

    slopes = [
        (1, 1),
        (3, 1),
        (5, 1),
        (7, 1),
        (1, 2),
    ]
    print("Part2: ", reduce(mul, [test_slope(tree_map, i) for i in slopes], 1))
