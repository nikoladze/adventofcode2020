#!/usr/bin/env python

import re

# Part 1
required_keys = [
    "byr",
    "iyr",
    "eyr",
    "hgt",
    "hcl",
    "ecl",
    "pid",
]

with open("input") as f:
    data = f.read()

n_valid = 0
for passport in data.split("\n\n"):
    present_keys = []
    for entry in passport.split():
        k, v = entry.split(":")
        present_keys.append(k)
    if all([k in present_keys for k in required_keys]):
        n_valid += 1
print("Part1:", n_valid)

# Part 2
def validate_height(x):
    if x.endswith("cm") and (150 <= int(x[:-2]) <= 193):
        return True
    if x.endswith("in") and (59 <= int(x[:-2]) <= 76):
        return True
    return False

valid_eye_colors = ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]

validators = {
    "byr" : lambda x: 1920 <= int(x) <= 2002,
    "iyr" : lambda x: 2010 <= int(x) <= 2020,
    "eyr" : lambda x: 2020 <= int(x) <= 2030,
    "hgt" : validate_height,
    "hcl" : lambda x: re.match("^#[0-9a-f]{6}$", x),
    "ecl" : lambda x: x in valid_eye_colors,
    "pid" : lambda x: re.match("^[0-9]{9}$", x),
    "cid" : lambda x: True,
}

def validate_passport(passport):
    present_keys = []
    for entry in passport.split():
        k, v = entry.split(":")
        if not validators[k](v):
            return False
        present_keys.append(k)
    if not all([k in present_keys for k in required_keys]):
        return False
    return True

n_valid = 0
for passport in data.split("\n\n"):
    if validate_passport(passport):
        n_valid += 1
print("Part2:", n_valid)
