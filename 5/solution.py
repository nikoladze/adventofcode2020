#!/usr/bin/env python


def binary_search(lr_list, low, high):
    if (high - low) == 1:
        return low if lr_list[0] else high
    n_left = (high - low) // 2
    if lr_list[0]:
        return binary_search(lr_list[1:], low, low + n_left)
    else:
        return binary_search(lr_list[1:], low + n_left + 1, high)


def decode_boarding_pass(boarding_pass):
    row_encoding = [1 if i == "F" else 0 for i in boarding_pass[:7]]
    col_encoding = [1 if i == "L" else 0 for i in boarding_pass[7:]]
    return binary_search(row_encoding, 0, 127), binary_search(col_encoding, 0, 7)


def seat_id(row, col):
    return row * 8 + col


def test_FBFBBFF_binary():
    assert binary_search([1, 0, 1, 0, 0, 1, 1], 0, 127) == 44


def test_FBFBBFFRLR():
    assert decode_boarding_pass("FBFBBFFRLR") == (44, 5)


def test_decode():
    assert decode_boarding_pass("BFFFBBFRRR") == (70, 7)
    assert decode_boarding_pass("FFFBBBFRRR") == (14, 7)
    assert decode_boarding_pass("BBFFBBFRLL") == (102, 4)


if __name__ == "__main__":
    with open("input") as f:
        boarding_passes = f.readlines()
    seat_ids = [
        seat_id(*decode_boarding_pass(boarding_pass))
        for boarding_pass in boarding_passes
    ]
    print("Part1:", max(seat_ids))

    print(
        "Part2:",
        [
            seat_id
            for seat_id in range(min(seat_ids), max(seat_ids))
            if not seat_id in seat_ids
        ][0],
    )
