#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>

#define INPUT_FILENAME "input"
#define MAX_GROUPLEN 32
#define NUM_LETTERS 26

int get_integer_representaton(const char *answer_string) {
  int i = 0;
  int start = (int)('a');
  for (int ic = 0; ic < strlen(answer_string); ++ic) {
    i |= (1 << ((int)answer_string[ic] - start));
  }
  return i;
}

int count_answers(int answer_integer) {
  int count = 0;
  for (int i = 0; i < NUM_LETTERS; ++i) {
    if ((answer_integer & (1 << i)) != 0)
      count +=1;
  }
  return count;
}

int sum_op_true(char group_answers[MAX_GROUPLEN][NUM_LETTERS], size_t group_len, int op_all) {
  int all = -1;
  int answer_integer;
  for (int i = 0; i < group_len; ++i) {
    answer_integer = get_integer_representaton(group_answers[i]);
    if (all == -1) {
      all = answer_integer;
    } else {
      if (op_all) {
        /* all */
        all &= answer_integer;
      } else {
        /* any */
        all |= answer_integer;
      }
    }
  }
  return count_answers(all);
}

int sum_all_true(char group_answers[MAX_GROUPLEN][NUM_LETTERS], size_t group_len) {
  return sum_op_true(group_answers, group_len, 1);
}

int sum_any_true(char group_answers[MAX_GROUPLEN][NUM_LETTERS], size_t group_len) {
  return sum_op_true(group_answers, group_len, 0);
}

int main()
{
  FILE *fp = fopen(INPUT_FILENAME, "r");
  if (!fp) {
    printf("Can't open file with filename \"%s\"\n", INPUT_FILENAME);
    return 1;
  }
  char group_answers[MAX_GROUPLEN][NUM_LETTERS];
  char *line = NULL;
  size_t line_len = 0;
  size_t n = 0;
  int i = 0;
  int total = 0;
  int total_any = 0;
  while ((line_len = getline(&line, &n, fp)) != -1) {
    if (line_len != 1) {
      line[line_len - 1] = '\0';
      strcpy(group_answers[i++], line);
    } else {
      total += sum_all_true(group_answers, i);
      total_any += sum_any_true(group_answers, i);
      i = 0;
    }
  }
  if (i > 0) {
    /*  final group */
    total += sum_all_true(group_answers, i);
    total_any += sum_any_true(group_answers, i);
  }
  printf("Part1: %d\n", total_any);
  printf("Part2: %d\n", total);
  fclose(fp);
  return 0;
}
