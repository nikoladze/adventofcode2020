#!/usr/bin/env python


def get_integer_representaton(answer_string):
    i = 0
    start = ord("a")
    for char in answer_string:
        i |= 1 << (ord(char) - start)
    return i


def count_answers(answer_integer):
    count = 0
    for i in range(26):
        if (answer_integer & (1 << i)) != 0:
            count += 1
    return count


def sum_all_true(groups):
    total = 0
    for group in groups:
        all_true = None
        for answer in group.strip().split("\n"):
            answer_integer = get_integer_representaton(answer.strip())
            if all_true is None:
                all_true = answer_integer
            else:
                all_true &= answer_integer
        total += count_answers(all_true)
    return total


def test_example():
    content = """
abc

a
b
c

ab
ac

a
a
a
a

b
"""
    groups = content.split("\n\n")
    assert sum_all_true(groups) == 6


if __name__ == "__main__":
    with open("input") as f:
        content = f.read().strip()
    groups = content.split("\n\n")
    print("Part1:", sum([len(set(group.replace("\n", ""))) for group in groups]))
    print("Part2:", sum_all_true(groups))
