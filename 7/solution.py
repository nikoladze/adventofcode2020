#!/usr/bin/env python


def parse_content_desc(content_str):
    content_list = []
    for entry in content_str.split(","):
        fields = entry.split()
        if fields[0] != "no":
            content_list.append(
                {"quantity": int(fields[0]), "name": " ".join(fields[1:3])}
            )
    return content_list


def parse_input_lines(input_file):
    bag_dict = {}
    for l in input_file:
        if len(l.strip()) == 0:
            continue
        container_str, content_str = l.strip().split("contain")
        container = " ".join(container_str.split()[:2])
        content = parse_content_desc(content_str)
        bag_dict[container] = content
    return bag_dict


def parse_filename(input_filename):
    with open(input_filename) as f:
        return parse_input_lines(f)


def can_contain(bag, other_bag, bag_dict):
    if len(bag_dict[bag]) == 0:
        return False
    elif any(
        [
            can_contain(content_bag["name"], other_bag, bag_dict)
            or content_bag["name"] == other_bag
            for content_bag in bag_dict[bag]
        ]
    ):
        return True
    else:
        return False


def total_number_that_can_contain(bag, bag_dict):
    n = 0
    for container, content in bag_dict.items():
        if can_contain(container, bag, bag_dict):
            n += 1
    return n


def total_number_of_bags_contained(bag, bag_dict):
    n = 0
    for content in bag_dict[bag]:
        n += content["quantity"]
        n += content["quantity"] * total_number_of_bags_contained(
            content["name"], bag_dict
        )
    return n


def test_example():
    input_str = """light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.
"""
    bag_dict = parse_input_lines(input_str.split("\n"))
    assert total_number_that_can_contain("shiny gold", bag_dict) == 4


def test_example2():
    input_str = """shiny gold bags contain 2 dark red bags.
dark red bags contain 2 dark orange bags.
dark orange bags contain 2 dark yellow bags.
dark yellow bags contain 2 dark green bags.
dark green bags contain 2 dark blue bags.
dark blue bags contain 2 dark violet bags.
dark violet bags contain no other bags."""
    bag_dict = parse_input_lines(input_str.split("\n"))
    assert total_number_of_bags_contained("shiny gold", bag_dict) == 126


if __name__ == "__main__":

    bag_dict = parse_filename("input")

    print("Part1", total_number_that_can_contain("shiny gold", bag_dict))
    print("Part2", total_number_of_bags_contained("shiny gold", bag_dict))
