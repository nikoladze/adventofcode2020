#!/usr/bin/env python

class LoopException(Exception):
    pass

class Gameboy:

    def __init__(self, instructions, debug=False):
        self._acc_val = 0
        self._pos = 0
        self.instructions = instructions
        self.instruction_history = set()
        self.debug = debug

    def nop(self, *args):
        self._pos += 1

    def acc(self, val):
        self._acc_val += val
        self._pos += 1

    def jmp(self, val):
        self._pos += val

    def execute_instruction(self, instruction):
        if self.debug:
            print(instruction, self._pos, self._acc_val)
        if instruction in self.instruction_history:
            raise LoopException(f"Loop detected - instruction, pos, acc_val: {(instruction, self._pos, self._acc_val)}")
        line_number, op, arg = instruction
        getattr(self, op)(arg)
        self.instruction_history.add(instruction)

    def run(self):
        while self._pos < len(self.instructions):
            self.execute_instruction(self.instructions[self._pos])


def read_instructions_from_file(filename):
    instructions = []
    with open(filename) as f:
        for i, l in enumerate(f):
            op, arg = l.strip().split()
            arg = int(arg)
            instructions.append((i, op, arg))
    return instructions


def test_example():
    instructions = [
        ("nop", +0),
        ("acc", +1),
        ("jmp", +4),
        ("acc", +3),
        ("jmp", -3),
        ("acc", -9),
        ("acc", +1),
        ("jmp", -4),
        ("acc", +6),
    ]
    instructions = [(i, op, arg) for i, (op, arg) in enumerate(instructions)]
    gb = Gameboy(instructions, debug=True)
    try:
        gb.run()
    except LoopException:
        assert gb._acc_val == 5


def try_replace_all(orig, target):
    for ins in [ins for ins in instructions if ins[1] == orig]:
        new_instructions = list(instructions)
        line_number, op, arg = ins
        new_instructions[line_number] = (line_number, target, arg)
        try:
            gb = Gameboy(new_instructions)
            gb.run()
        except LoopException:
            continue
        print(f"Found solution: replace {ins} by {(line_number, target, arg)}")
        print(f"Value of accumulator: {gb._acc_val}")

if __name__ == "__main__":

    instructions = read_instructions_from_file("input")

    try:
        gb = Gameboy(instructions)
        gb.run()
    except LoopException as e:
        print(e)

    try_replace_all("nop", "jmp")
    try_replace_all("jmp", "nop")



