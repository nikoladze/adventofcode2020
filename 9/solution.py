#!/usr/bin/env python

import numpy as np

array = np.loadtxt("input", dtype=np.int64)

for i in range(25, len(array)):
    prev_25 = array[i - 25: i]
    a, b = np.meshgrid(prev_25, prev_25)
    valid = set((a + b).ravel())
    if not array[i] in valid:
        first_invalid = array[i]

print("Part1: ", first_invalid)

for lower in range(len(array)):
    for upper in range(len(array)):
        if (lower + 1) >= upper:
            continue
        if array[lower: upper].sum() == first_invalid:
            print("Part2: ", array[lower: upper].min() + array[lower: upper].max())
